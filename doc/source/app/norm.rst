tomogui norm
------------

This small application take a single frame .edf file and perform the following operation:

   :math:`\frac{data}{data.max()}` and store the result in the second given file.

.. code-block:: bash

    tomogui invert fileDataToNorm.edf fileDataToSave.edf

