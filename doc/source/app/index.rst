applications
============

tomogui is offering a set of script in order to manage reconstructions.
All application can be launched from command line:

.. code-block:: bash

   tomogui [applicationName] [parameters]

This is the list of the existing applications

.. toctree::
   :maxdepth: 1

   project.rst
   recons.rst
   creator.rst
   materials.rst
   norm.rst
   invert.rst
