tomogui |version|
=================

tomogui is a graphical interface to run transmission and fluorescence reconstructions.

For now a main goal of tomogui is to generate project file for the freeart library (QFreeARTWidget).
A reconstruction interface is also accessible to interface freeart during the reconstruction (QFreeARTReconstruction).

.. toctree::
   :hidden:

   overview.rst
   install.rst
   tutorials.rst
   app/index.rst
   commonmistakes.rst
   developer.rst
   license.rst


:doc:`install`
    How to install *silx* on Linux, Windows and MacOS X

:doc:`tutorials`
    Tutorials and sample code

:doc:`app/index`
    tomogui project, tomogui recons, ...

:doc:`commonmistakes`
    Some hint that might help you if your reconstructions look strange...

:doc:`license`
    License and copyright information

:doc:`developer`
    Some extra information for developer
