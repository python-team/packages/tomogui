#!/bin/bash

function install_freeart(){

    if [ "$2" = 'latest' ]; then
        pip install freeart
    else
        unset http_proxy
        unset https_proxy
        mkdir freeart
        cd freeart
        wget https://gitlab.esrf.fr/freeart/freeart/repository/"$2"/archive.tar
        tar -xvf archive.tar
        rm archive.tar
        cd ./*
        # fix: don't know why but the 0.29 version bring lost of the Geometry.cpp path
        python -m pip install Cython==0.28
        python -m pip install -r requirements.txt --upgrade
        python -m pip install .
        cd ../../
        export http_proxy=http://proxy.esrf.fr:3128/
        export https_proxy=http://proxy.esrf.fr:3128/
    fi
}


function freeart_version(){
    python -c 'import freeart; print(freeart.version)'
}
