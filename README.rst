Tomogui
#######

Tomogui offers a Graphical User Interface to run tomographic reconstruction based on freeart (https://gitlab.esrf.fr/freeart/freeart) and silx (https://github.com/silx-kit/silx) algorithms

Installation
------------

pip install tomogui

Usage
-----

Tomogui contains two main applications:

- tomogui project : to create a new project from scratch
- tomogui recons : to run reconstruction from a configuration file created by 'tomogui project'

See more in `documentation <http://www.edna-site.org/pub/doc/tomogui/latest/app/index.html>`_.

Documentation
-------------

Documentation of latest release is available at http://www.edna-site.org/pub/doc/tomogui/latest


License
-------

The source code of tomogui is licensed under the MIT license.
See the `LICENSE <https://gitlab.esrf.fr/tomoTools/tomogui/blob/master/LICENSE>`_ and
`copyright <https://gitlab.esrf.fr/tomoTools/tomogui/blob/master/copyright>`_ files for details.
